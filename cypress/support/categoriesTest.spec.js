/// <reference types="cypress" />

Cypress.Commands.add('categories', () => {
    cy.get(`.categories a.category`).each(element => {
        const url = element[0].innerHTML === 'All' ? '/' : `/search/${element[0].innerHTML}`;
        const nameElement = element[0].innerHTML === 'All' ? '' : element[0].innerHTML;
        
        cy.get(element).click().wait(500).url().should('include', url)
            .get(".tovar p").first().should('contain.text', nameElement);
    });
});
